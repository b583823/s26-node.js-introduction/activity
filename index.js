
let http = require("http");
const port = 3000;
http.createServer(function(request, response){
	if(request.url == '/login'){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.end("You are in the login page.");
	}
	else{
		response.writeHead(404, {"Content-Type":"text/plain"});
		response.end("Page is not available.");
	}

}).listen(port);
console.log("Server running at localhost:3000");