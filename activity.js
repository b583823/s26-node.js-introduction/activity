// What directive is used by Node.js in loading the modules it needs?
ANSWER: HTTP modules

// - What Node.js module contains a method for server creation?
ANSWER: createServer

// - What is the method of the http object responsible for creating a server using Node.js?
ANSWER: request and response

// - What method of the response object allows us to set status codes and content types?
ANSWER: writeHead

// - Where will console.log() output its contents when run in Node.js?
ANSWER: response.end

// - What property of the request object contains the address's endpoint?
ANSWER: listen(port)